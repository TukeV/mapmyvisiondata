from matplotlib import pyplot as plt
import sqlite3
import datetime
import matplotlib.colors as mcolors


def prepare_figure(title):
    """
    Creates the figure and axis objects and sets the face colors and the window title.
    :param title: Title of the figure.
    :return: figure and the axis
    """
    figure = plt.figure()
    figure.set_facecolor("grey")
    axis = figure.add_subplot(111, projection='polar')
    axis.set_facecolor("xkcd:dark grey")
    axis.set_title(title)
    return figure, axis


def add_plot(connection, session_info, axis, color):
    """
    Fetches the data from the datagbase and plots it.
    :param connection: sqlite3 connection to the database
    :param session_info: session data from the session table, fetched in the previous query.
    :param axis: Matplotlib axis object, where the plots are drawn.
    :param color: Color which plots will use. This is needed because the function is going to draw
    2 plots; one for the actual data and one for windowed average.
    """
    cursor = connection.cursor()

    # Add raw data
    raw_data = cursor.execute(
        "SELECT Polar, FoV "
        "FROM MEASUREMENTS "
        "WHERE SessionID=?", (session_info["id"],)).fetchall()
    raw_data = tuple(map(list, zip(*raw_data)))
    data_args = {
        "marker": "o",
        "markersize": 20 * session_info["size"],
        "color": color,
        "mfc": color if session_info["direction"] == 0 else "xkcd:dark grey",
        "label": "Direction: {}\nSpeed: {}\nSize: {}".format(
            session_info["direction"],
            round(session_info["speed"], 1),
            round(session_info["size"], 1),
        ),
        "linestyle": ""
    }
    axis.plot(*raw_data, **data_args)

    # Add the average
    windowed_average = cursor.execute(
        "SELECT Polar, FoV "
        "FROM WINDOWED_AVERAGE "
        "WHERE SessionID=?", (session_info["id"],)).fetchall()
    windowed_average = tuple(map(list, zip(*windowed_average)))
    wa_args = {
        "marker": None,
        "color": color,
        "linestyle": "--",
    }
    # The windowed is still missing the last line segment ie. the line between the last and the first point.
    # This can be fixed by adding the first coordinates to the end of the list.
    windowed_average[0].append(windowed_average[0][0])
    windowed_average[1].append(windowed_average[1][0])
    axis.plot(*windowed_average, **wa_args)


def fetch_and_draw_recent():
    """
    Draws a polar coordinate plot, which includes all the measurements
    from the latest date found from the database.
    """
    conn = sqlite3.connect("visiondata.db")
    sessions, date = fetch_most_recent_sessions(conn)
    fig, axis = prepare_figure(date.strftime("%d.%m.%Y"))
    color_list = list(mcolors.TABLEAU_COLORS)
    for i, s in enumerate(sessions):
        color = color_list[i % len(color_list)]
        add_plot(conn, s, axis, color)
    axis.legend()
    plt.show()


def fetch_most_recent_sessions(connection):
    """
    Fetches and parses the session info of all the measurements made
    in the latest date found from the database.
    :param connection: Connection to the sqlite database
    :return: Function returns two objects.
    1) List of dictionaries. Each dictionary contains the session id, direction, speed
    and the size information about the measurement.
    2) Date object, which has the last date found from the database.
    """
    cursor = connection.cursor()
    command = "SELECT id, Direction, Speed, Size, Timestamp " \
              "FROM SESSIONS " \
              "WHERE DATE(Timestamp) = (SELECT MAX(DATE(Timestamp)) FROM SESSIONS)"
    result = cursor.execute(command).fetchall()
    date = datetime.datetime.strptime(result[0][-1], "%Y-%m-%d %H:%M:%S").date()
    return [{"id": int(r[0]), "direction": r[1], "speed": r[2], "size": r[3]} for r in result], date


if __name__ == '__main__':
    fetch_and_draw_recent()
