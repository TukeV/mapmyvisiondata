import matplotlib.pyplot as plt
import datetime
import sqlite3
from estimate_fov_area import group_data, regression
from matplotlib import colors as mplcolors
from matplotlib import cm

# Fetch data
db = sqlite3.connect("visiondata.db")
cursor = db.cursor()
cmd = "SELECT SESSIONS.Timestamp, SESSIONS.Speed, SESSIONS.Size, MEASUREMENTS.Polar, MEASUREMENTS.FoV " \
      "FROM MEASUREMENTS INNER JOIN SESSIONS ON MEASUREMENTS.SessionID=SESSIONS.ID " \
      "WHERE SESSIONS.Direction=0 " \
      "ORDER BY SESSIONS.Timestamp"
cursor.execute(cmd)
raw_data = cursor.fetchall()

# Group it
data = group_data(raw_data)

# Prepare figure
figure = plt.figure()
figure.set_facecolor("grey")
axis = figure.add_subplot(211)
axis.set_facecolor("darkgray")

# Regression analysis
predictors, areas = regression(*data)
times = list(predictors[:, 0])
dates = [datetime.datetime.fromtimestamp(ts) for ts in data[0]]
predict_dates = [datetime.datetime.fromtimestamp(ts) for ts in list(times)]

# Plot with speed
norm = mplcolors.Normalize(min(data[1]), max(data[1]))
norm_colors = norm(data[1])
colors = plt.cm.jet(norm_colors)

axis.scatter(dates, data[-1], label="Mittaus", edgecolor='black', zorder=2, c=colors)
axis.plot(predict_dates, areas, ":", linewidth=4, label="Trendi", color="black", zorder=1)

# Decorate axis
axis.set_ylim(bottom=0)
axis.legend()
axis.set_xlabel("PVM")
axis.set_ylabel("Pinta-ala")
axis.minorticks_on()
axis.set_title("Näkökentän pinta-alan kehitys")
axis.grid(True, axis="both", which="both", zorder=0, color="black")
plt.yticks()
plt.xticks()
cbar = figure.colorbar(cm.ScalarMappable(norm=norm, cmap='jet'), ax=axis)
cbar.ax.set_ylabel("Speed")


# Plot with sphere size
axis = figure.add_subplot(212)
axis.set_facecolor("darkgray")

norm = mplcolors.Normalize(min(data[2]), max(data[2]))
norm_colors = norm(data[2])
colors = plt.cm.jet(norm_colors)

axis.scatter(dates, data[-1], label="Mittaus", edgecolor='black', zorder=2, c=colors)
axis.plot(predict_dates, areas, ":", linewidth=4, label="Trendi", color="black", zorder=1)

# Decorate axis
axis.set_ylim(bottom=0)
axis.legend()
axis.set_xlabel("PVM")
axis.set_ylabel("Pinta-ala")
axis.minorticks_on()
axis.set_title("Näkökentän pinta-alan kehitys")
axis.grid(True, axis="both", which="both", zorder=0, color="black")
plt.yticks()
plt.xticks()
cbar = figure.colorbar(cm.ScalarMappable(norm=norm, cmap='jet'), ax=axis)
cbar.ax.set_ylabel("Size")

plt.show()

