# README #

This repository contains the python scripts, which can be used to analyze and visualize the data gathered from the [MapMyVision VR App](https://bitbucket.org/TukeV/mapmyvision/).


### How to use? ###

Since these scripts are meant for private use, getting started can be a bit painful.

#### Data ####

Unfortunately this repository does not yet contain example data which could be used to demonstrate what each script does. The user has to generate their own data set in order to use these scripts.

#### Requirements ####

These python scripts depends on following external libraries and it's recommended to install the latest versions of these modules.

    - numpy
    - pandas
    - matplotlib
    - scikit-learn

The requirements can be installed with following command.

    python3 -m pip install -r requirements.txt

#### Initializing the Database ####

The sqlite database file must be initialized before using the scripts. This can be done by applying the `init.sql` to the database file.

    > sqlite3 visiondata.db
    ...
    sqlite> .read init.sql
    ...

#### Directory Structure ####

The scripts will try to look for the input data from the following directories, so it might be good idea to prepare these directories beforehand before fetching the data.

    .
    +-- Data
    +-- appearing_data
    +-- disappearing_data
    +-- merged_data


#### Usage ####

In windows you can use ´pipeline.ps1 -visualize´ to draw the visualizations for the files. This behaviour is also enabled in bash script ´demo.sh´.

If user wants to also fetch the most data from the headset, the user can call ´pipeline.ps1 -full´ to fetch data from the headset, save it to the directories, visualize it and the remove the now redundant files from the headset.

For more help, try calling ´Get-Help pipeline.ps1´.



    
   