import sqlite3
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sstats
from itertools import groupby
from windowing_algorithms import windowed_average


def get_last_month_data(direction=0, max_size=.2):
    """
    Fetches the results starting from the most recent measurement and ending from 30 days before that.
    :param direction: Direction which measurements are fetched from. 0 is for spheres moving towards the center
    and 1 is for spheres moving away from the center.
    :param max_size: Filters data by the maximum size of the sphere.
    Tests where sphere size was larger will be excluded.
    :return: List of (polar, fovs) tuples, ordered by the polar angle.
    """
    connection = sqlite3.connect("visiondata.db")
    cursor = connection.cursor()
    command = "SELECT MEASUREMENTS.Polar, MEASUREMENTS.FoV " \
              "FROM MEASUREMENTS INNER JOIN SESSIONS ON SessionID=ID " \
              "WHERE DATE(Timestamp) > DATE((SELECT MAX(DATE(Timestamp)) FROM SESSIONS), '-1 month', '-1 day') " \
              "AND Direction=? AND Size<=?" \
              "ORDER BY MEASUREMENTS.Polar, MEASUREMENTS.FoV"
    cursor.execute(command, (direction, max_size))
    data = cursor.fetchall()
    return data


def calculate_last_month_average(data):
    """
    Calculates the average field of view for each polar distinct angle in the data.
    :param data: List of (polar, fov) tuples
    :return: lists of distinct polar_angles and average field of views.
    """
    avg_polars, avg_fovs = [], []
    for polar, fov_list in groupby(data, lambda d: d[0]):
        fov_list = list(fov_list)
        _, fovs = tuple(map(list, zip(*fov_list)))
        avg_polars.append(polar)
        avg_fovs.append(sum(fovs)/len(fovs))
    return avg_polars, avg_fovs


def confidence_interval(data, level=.90):
    """
    Calculates the means and confidence intervals for each polar coordinate.
    :param data: Data queried from the database. See :func:`calculate_last_month_average`
    :param level: Sets the confidence level to be calculated. Default is .90 (ie. 90%)
    :return: lists for means, lower values of the confidence interval and higher values of the confidence interval
    """
    lows, means, highs = [], [], []
    for polar, fov_list in groupby(data, lambda d: d[0]):
        fov_list = list(fov_list)
        _, fovs = tuple(map(list, zip(*fov_list)))
        mean = np.mean(fovs)
        low, high = sstats.t.interval(alpha=level, df=len(fovs)-1, loc=mean, scale=sstats.sem(fovs))
        lows.append(low)
        highs.append(high)
        means.append(mean)
    return means, lows, highs


def prepare_figure():
    """
    Creates the figure with one polar coordinate area.
    :return: matplotlib figure and axis objects
    """
    figure, axis = plt.subplots(1, 1, subplot_kw=dict(projection='polar'), figsize=[12, 8])
    figure.set_facecolor("grey")
    axis.set_facecolor("xkcd:dark grey")
    figure.suptitle("Test Results For Last 30 Days", fontsize=14)
    return figure, axis


def plot_confidence(axis, polars, means, lows, highs, level, dataset_name, color):
    polars.append(polars[0])
    means.append(means[0])
    lows.append(lows[0])
    highs.append(highs[0])
    axis.plot(polars, means, '.', color=color)
    axis.plot(polars, lows, ':', color=color, label=f"{level}% Confidence for {dataset_name}")
    axis.plot(polars, highs, ':', color=color)


def plot_last_month_average(axis, polars, fovs, label, color):
    """
    Plots the polar coordinates and their windowed average to the matplotlib axis.
    :param axis: Axis where the data will be plotted
    :param polars: List of angles of the polar coordinates. The list must be as long as the list of ranges.
    :param fovs: List of field of views, ie. list of ranges for polar coordinates. Must be as long as list of angles.
    :param label: Text label shown in the legend
    :param color: Color of the plot
    """
    # Scatter plot for the averages
    axis.plot(polars, fovs, 'x', color=color)
    # Line plot for windowed average
    if len(fovs) > 11:
        w_avg = windowed_average(fovs)
        w_avg.append(w_avg[0])
        tmp = [p for p in polars]
        tmp.append(polars[0])
        axis.plot(tmp, w_avg, '-', color=color, label=f"Windowed Average for {label} ")


def main():
    """
    Draws the 30 day average of both the appearing spheres and the disappearing spheres tests and their
    windowed averages. Data is fetched from the database called "visiondata.db"
    """
    figure1, axis = prepare_figure()
    # Plot appearing spheres
    data = get_last_month_data()
    avg_polars, avg_fovs = calculate_last_month_average(data)
    means, lows, highs = confidence_interval(data, .90)
    plot_last_month_average(axis, avg_polars, avg_fovs, "Appear", "cyan")
    plot_confidence(axis, avg_polars, means, lows, highs, 90, "Appear", "lime")

    # Plot disappearing spheres
    data = get_last_month_data(1, .5)
    avg_polars, avg_fovs = calculate_last_month_average(data)
    means, lows, highs = confidence_interval(data, .90)
    plot_last_month_average(axis, avg_polars, avg_fovs, "Disappear", "darkorange")
    plot_confidence(axis, avg_polars, means, lows, highs, 90, "Disappear", "red")
    axis.legend(bbox_to_anchor=(1.4, 1.), loc="upper right")
    plt.show()


if __name__ == '__main__':
    main()
