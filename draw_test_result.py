import matplotlib.pyplot as plt
import argparse
from windowing_algorithms import weighted_windowed_average, windowed_average
import glob
import os
import re


def read_file(filename):
    """
    Reads the data from the given file. The reader will omit lines which have //
    in the beginning of the line.
    :param filename: Path where the file is.
    :return: two lists, where first has the polar coordinate angle
    and second one is the distance from the origo.
    """
    polar_angles = []
    fov_angles = []
    with open(filename, 'r') as datafile:
        for line in datafile:
            if line[:2] == "//":
                continue
            point = [float(x) for x in line.split(',')]
            polar_angles.append(point[0])
            fov_angles.append(point[-1])
    return polar_angles, fov_angles


def draw_file(data_file, ax, scatter_options, avg_options, fill_options):
    """
    Reads the file and draws it's content
    :param data_file: Source file of the data
    :param ax: Matplotlib Axis object, where plot will be drawn
    :param scatter_options: Arguments for the individual data points. See matplotlib documentation for more information.
    :param avg_options: Arguments for drawing the line plot of the smoothed average (ie. windowed average)
    :param fill_options: Options for pyplot.fill_between function. Relevant only if avg options are also provided
    :return:
    """
    angles, fovs = read_file(data_file)
    if scatter_options is not None:
        ax.plot(angles, fovs, **scatter_options)
    if avg_options is not None:
        angles.append(angles[0])
        fovs.append(fovs[0])
        wa_data = windowed_average(fovs, 7)
        ax.plot(angles, wa_data, **avg_options)
        if fill_options is not None:
            plt.fill_between(angles, wa_data, **fill_options)


def add_scatter_plot(data_file, label, ax):
    """
    Draws the scatter plot of the raw values found from the data file
    :param data_file: Source of the data
    :param label: label for the plot
    :param ax: Axis where the plot is drawn
    """
    data = read_file(data_file)
    ax.plot(data[0], data[1], 'o', label=label)


def add_line_plot(data_file, label, ax, fillcolor='dimgray'):
    """
    Reads the data file and draws a line plot based on it's content
    :param data_file: Source of the data
    :param label: label of the line plot
    :param ax: Axis where the line plot will be drawn
    :param fillcolor: Fill color between the axis value and origo
    :return:
    """
    angles, fovs = read_file(data_file)
    angles.append(angles[0])
    fovs.append(fovs[0])
    wa_data = windowed_average(fovs, 7)
    ax.plot(angles, wa_data, label=label)
    plt.fill_between(angles, wa_data, color=fillcolor)


def prepare_figure(title):
    """
    Prepares the figure with one axis using polar coordinates.
    :param title: Title of the figure
    :return: Figure and Axis objects
    """
    fig = plt.figure()
    fig.set_facecolor("grey")
    ax = fig.add_subplot(111, projection='polar')
    ax.set_facecolor("xkcd:dark grey")
    plt.title(title)
    return fig, ax


def show_figure():
    """Handles the final details such as legend and the calls plt.show"""
    plt.legend()
    plt.show()


def draw_daily_result(appear_file, disappear_file, avg_file, title):
    """
    Draws a single daily result.
    :param appear_file: Path to results of appearing spheres test
    :param disappear_file: Path to results of disappearing spheres test
    :param avg_file: Path to file containing the averages of the two previous results
    :param title: Title for the graph.
    """
    fig, ax = prepare_figure(title)
    if disappear_file is not None:
        draw_file(disappear_file, ax,
              {"marker": "o", "color": "C1", "label": "Disappear", "linestyle": ""},
              {"marker": None, "linestyle": "--", "color": "C1"},
              {"color": "wheat"})
    if appear_file is not None:
        draw_file(appear_file, ax,
              {"marker": "o", "color": "C2", "label": "Appear", "linestyle": ""},
              {"marker": None, "linestyle": "--", "color": "C2"},
              {"color": "lightgreen"})
    if avg_file is not None:
        draw_file(avg_file, ax, None, {"linestyle": "-", "color": "C3", "label": "Average"}, None)
    show_figure()


def draw_6_results():
    """
    Method to draw 6 latest test results. The function will draw the results in a 2x3 grid.
    """
    fig = plt.figure("6 Last results")
    fig.set_facecolor("grey")
    rows = 2
    columns = 3
    results = daily_file_generator()
    for i in range(1, 7):
        title, into_file, outto_file, merge_file = next(results)
        ax = fig.add_subplot(rows, columns, i, projection='polar')
        if outto_file is not None:
            draw_file(outto_file, ax,
                      {"marker": "o", "color": "C1", "label": "Disappear", "linestyle": ""},
                      {"marker": None, "linestyle": "--", "color": "C1"},
                      {"color": "wheat"})
        if into_file is not None:
            draw_file(into_file, ax,
                      {"marker": "o", "color": "C2", "label": "Appear", "linestyle": ""},
                      {"marker": None, "linestyle": "--", "color": "C2"},
                      {"color": "lightgreen"})
        if merge_file is not None:
            draw_file(merge_file, ax, None, {"linestyle": "-", "color": "C3", "label": "Average"}, None)
        ax.set_facecolor("xkcd:dark grey")
        ax.set_title(title)
    plt.show()


def auto_draw(n):
    """
    Finds N most recent files dated to same date in appearing, disappearing and merged data and draws their results in
    separate windows. New file is drawn after previous window is closed.
    :param n: How many files are drawn
    """
    iterator = 0
    result_generator = daily_file_generator()
    while iterator < n:
        title, into, outto, merged = next(result_generator)
        draw_daily_result(into, outto, merged, title)
        iterator += 1


def daily_file_generator():
    into_files = sorted(glob.glob("./appearing_data/*.csv"), reverse=True)
    outto_files = sorted(glob.glob("./disappearing_data/*.csv"), reverse=True)
    merged_files = sorted(glob.glob("./merged_data/*.csv"), reverse=True)
    dates = []
    for s in (outto_files + into_files + merged_files):
        dates.append(os.path.basename(s).split("_")[0])
    dates = sorted(list(set(dates)), reverse=True)
    i_into, i_outto, i_merged = 0, 0, 0
    for d in dates:
        into_prefix = os.path.basename(into_files[i_into]).split("_")[0]
        outto_prefix = os.path.basename(outto_files[i_outto]).split("_")[0]
        merged_prefix = os.path.basename(merged_files[i_merged]).split("_")[0]
        current_into = into_files[i_into] if into_prefix == d else None
        current_outto = outto_files[i_outto] if outto_prefix == d else None
        current_merged = merged_files[i_merged] if merged_prefix == d else None
        if current_into is not None:
            i_into += 1
        if current_outto is not None:
            i_outto += 1
        if current_merged is not None:
            i_merged += 1
        [year, month, day] = re.findall("\d\d", d)
        title = "{}.{}.20{}".format(day, month, year)
        yield title, current_into, current_outto, current_merged


def get_args():
    """
    Prepares the argument parser. Calling this script with --help will give the detailed explanation for
    all the commandline arguments
    :return: Parsed arguments, see documentation of Argument.Parser.parse_args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--draw-single", nargs=1, type=str, required=False,
                        help="Use this argument to draw only a single file.")
    parser.add_argument("-sf", "--scatter_files", nargs="+", type=str, required=False,
                        help="These files will be drawn as scatter plots")
    parser.add_argument("-lf", "--line_files", nargs="+", type=str, required=False,
                        help="These files will be drawn as line plots")
    parser.add_argument("--labels", nargs="+", type=str, required=False,
                        help="Custom legends for the drawn data")
    parser.add_argument("--title", type=str, help="Title for the chart")
    parser.add_argument("--number", type=int, required=False, help="Determines how many test results the script should draw."
                                                                   "Each result is drawn on it's own window.")
    parser.add_argument("--draw6", action="store_true", required=False, default=False,
                        help="Automatically search and match files 6 most recent files from /disappearing_data/ and /into/data directories. "
                             "Otherwise the script will draw only the most recent test results.")
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    if args.draw6:
        draw_6_results()
    elif args.number is not None:
        auto_draw(args.number)
    elif args.draw_single is not None:
        filename = os.path.basename(args.draw_single[0])
        draw_daily_result(args.draw_single[0], None, None, f"Results of the file {filename}")
    else:
        auto_draw(1)
