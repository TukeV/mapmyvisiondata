import sqlite3
from estimate_fov_area import group_data
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as colormap


def fetch_data(connection):
    """
    Fetches the measurements and the measurmenet info from the database
    :param connection: sqlite3 connection object
    :return: Data queried from the database
    """
    cursor = connection.cursor()
    command = "SELECT SESSIONS.Timestamp, SESSIONS.Speed, SESSIONS.Size, " \
              "MEASUREMENTS.Polar, MEASUREMENTS.FoV " \
              "FROM MEASUREMENTS INNER JOIN SESSIONS ON MEASUREMENTS.SessionID=SESSIONS.ID " \
              "WHERE SESSIONS.Direction=0 " \
              "ORDER BY SESSIONS.Timestamp"
    cursor.execute(command)
    data = cursor.fetchall()
    return data


def plot_3d_scatter(data):
    """
    Draws a 3d scaatter plot from the data. X axis is the sphere speed, Y Axis is the sphere size
    and the Z axis is the area measured in that test. The marker size will scale based on
    the sphere size and the marker color will vary based on the sphere speed.
    :param data: A list created by the estimate_fov_area.group_data function
    """
    fig = plt.figure()
    fig.set_facecolor("grey")
    ax = fig.add_subplot(projection='3d')
    # Make markers size scale based on sphere size
    sizes = [s * 250 for s in data[2]]
    # Make marker colors vary based on sphere speed

    # Draw!
    ax.scatter(data[1], data[2], data[3], s=sizes, c=data[1], cmap='jet')
    ax.set_xlabel("Speed")
    ax.set_ylabel("Size")
    ax.set_zlabel("Visible Area")
    ax.minorticks_on()
    ax.grid(True, axis="both", which="both", zorder=1)
    ax.set_title("Sphere's Speed and Size vs. Measured Area")
    plt.show()


if __name__ == '__main__':
    connection = sqlite3.connect("visiondata.db")
    data = fetch_data(connection)
    data = group_data(data)
    plot_3d_scatter(data)
