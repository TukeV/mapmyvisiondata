import sqlite3
from windowing_algorithms import windowed_average


def find_missing_sessions(connection):
    """
    Finds the sessions which are not present in the WINDOWED_AVERAGE table
    :param connection: Connection to the database
    :return: All the missing Session IDs which are present in Sessions but not in WINDOWED_AVERAGE table
    """
    cursor = connection.cursor()
    command = "SELECT ID FROM SESSIONS WHERE NOT EXISTS(" \
              "SELECT SessionID FROM WINDOWED_AVERAGE WHERE SESSIONS.ID = WINDOWED_AVERAGE.SessionID)"
    result = cursor.execute(command).fetchall()
    return result


def get_session_data(connection, session):
    """
    Queries the data related to the session id
    :param connection: Connection to the database
    :param session: Session ID to be queried
    :return: polar and field of views associated to the session ID
    """
    cursor = connection.cursor()
    command = "SELECT Polar, FoV FROM MEASUREMENTS " \
              "WHERE MEASUREMENTS.SessionID =?"
    session_data = cursor.execute(command, session).fetchall()
    polars, fovs = list(map(list, zip(*session_data)))
    return polars, fovs


def insert2db(connection, session_id, polars, wa_fovs):
    """
    Stores windowed averages into WINDOWED_AVERAGE table
    :param connection: Database connection
    :param session_id: Session ID where windowed averages were calculated from
    :param polars: polar angles
    :param wa_fovs: field of views
    """
    cursor = connection.cursor()
    command = "INSERT INTO 'WINDOWED_AVERAGE' ('SessionID', 'Polar', 'FoV') VALUES (?, ?, ?);"
    data = list(map(lambda pf: [session_id, pf[0], pf[1]], zip(*[polars, wa_fovs])))
    cursor.executemany(command, data)


def wa2db(dbname):
    """
    Pre-calculates and stores windowed averages into database.
    :param dbname: Name of the database file
    """
    connection = sqlite3.connect(dbname)
    missing_sessions = find_missing_sessions(connection)
    for session in missing_sessions:
        session_id = session[0]
        polars, fovs = get_session_data(connection, session)
        wa_fovs = windowed_average(fovs)
        insert2db(connection, session_id, polars, wa_fovs)
    connection.commit()
    connection.close()


if __name__ == '__main__':
    wa2db("visiondata.db")
