#!/bin/bash

echo "Drawing the most recent results"
python3 draw_most_recent.py

echo "Draw the 30 day average"
python3 monthly_results.py

echo "Drawing the estimated progression"
python3 estimate_fov_area.py

echo "Drawing the estimated results of Appearing Spheres test in near future"
python3 future_fov.py

