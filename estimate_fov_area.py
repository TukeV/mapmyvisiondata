import numpy as np
from sklearn.linear_model import LinearRegression
from matplotlib import pyplot as plt
import datetime
import sqlite3
from itertools import groupby


def fetch_data(direction, size_limit):
    connection = sqlite3.connect("visiondata.db")
    cursor = connection.cursor()
    command = "SELECT SESSIONS.Timestamp, SESSIONS.Speed, SESSIONS.Size, WINDOWED_AVERAGE.Polar, WINDOWED_AVERAGE.FoV "\
              "FROM WINDOWED_AVERAGE INNER JOIN SESSIONS ON WINDOWED_AVERAGE.SessionID=SESSIONS.ID " \
              "WHERE SESSIONS.Direction=? AND SESSIONS.Size<? " \
              "ORDER BY SESSIONS.Timestamp"
    cursor.execute(command, (direction, size_limit,))
    data = cursor.fetchall()
    return data


def group_data(data):
    timestamps, speeds, sizes, areas = [], [], [], []
    for key, group in groupby(data, lambda x: x[0]):
        group = list(group)
        _, group_speeds, group_sizes, polars, fovs = tuple(map(list, zip(*group)))
        area = calculate_area(polars, fovs)
        timestamps.append(datetime.datetime.strptime(key, "%Y-%m-%d %H:%M:%S").timestamp())
        speeds.append(group_speeds[0])
        sizes.append(group_sizes[0])
        areas.append(area)
    return [timestamps, speeds, sizes, areas]


def calculate_area(polars, fovs):
    assert len(polars) == len(fovs)
    x = fovs * np.cos(polars)
    y = fovs * np.sin(polars)
    total_area = 0
    for i in range(0, len(polars)):
        total_area += np.fabs(x[i-1]*y[i] - x[i]*y[i-1])
    return total_area * .5


def regression(timestamps, speeds, sizes, areas):
    X = np.column_stack((timestamps, speeds, sizes))
    Y = np.array(areas)
    reg = LinearRegression()
    reg.fit(X, Y)
    estimation_speed = sum(speeds) / len(speeds)
    estimation_size = sum(sizes) / len(sizes)
    last_date = -(reg.coef_[1] * estimation_speed + reg.coef_[2] * estimation_size + reg.intercept_)/reg.coef_[0]
    time_predict = [timestamps[0], last_date]
    speed_predict = [estimation_speed, estimation_speed]
    size_predict = [estimation_size, estimation_size]
    x_predict = np.column_stack((time_predict, speed_predict, size_predict))
    y_predict = reg.predict(x_predict)
    return x_predict, y_predict


def add_plots(axis, timestamps, areas, x_predict, y_predict, label, color):
    dates = [datetime.datetime.fromtimestamp(ts) for ts in timestamps]
    predict_dates = [datetime.datetime.fromtimestamp(ts) for ts in list(x_predict)]

    axis.plot(dates, areas, "-o", label=label, color=color)
    axis.plot(predict_dates, y_predict, ":", label="Regression for {}".format(label), color=color)


def estimate_area_w_regression(axis, title, appear_parameters, disappear_parameters, filter=None):
    if filter is not None:
        for i, flag in enumerate(filter):
            if not flag:
                appear_parameters[i] = [0] * len(appear_parameters[i])
                disappear_parameters[i] = [0] * len(disappear_parameters[i])
    # Fetch data for appearing spheres and add it to the figure
    predictors, predicted_areas = regression(*appear_parameters)
    prediction_times = list(predictors[:, 0])
    add_plots(axis, appear_parameters[0], appear_parameters[-1], prediction_times, predicted_areas, "Appearing Spheres", "blue")
    # Fetch data for disappearing spheres and add it to the figure
    predictors, predicted_areas = regression(*disappear_parameters)
    prediction_times = list(predictors[:, 0])
    add_plots(axis, disappear_parameters[0], disappear_parameters[-1], prediction_times, predicted_areas, "Disappearing Spheres", "red")

    # Decorate axis
    axis.set_ylim(bottom=0)
    axis.legend()
    axis.set_xlabel("Time")
    axis.set_ylabel("Area")
    axis.minorticks_on()
    axis.set_title(title)
    axis.grid(True, axis="both", which="both", zorder=1)


def main():
    # prepare figure
    figure = plt.figure()
    figure.set_facecolor("grey")
    # Axis0: Estimation without speed or size
    # Fetch data
    appear_data = group_data(fetch_data(0, 0.4),)
    disappear_data = group_data(fetch_data(1, 0.6))
    axis = figure.add_subplot(211)
    estimate_area_w_regression(axis, "Regression with date and area", appear_data, disappear_data, filter=[True, False, False])
    # Axis1: Estimation based on all available data
    appear_data = group_data(fetch_data(0, 0.6),)
    disappear_data = group_data(fetch_data(1, 0.6))
    axis = figure.add_subplot(212)
    estimate_area_w_regression(axis, "Regression with all available data", appear_data, disappear_data, filter=[True, True, True])
    plt.show()


if __name__ == '__main__':
    main()
