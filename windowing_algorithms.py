import numpy as np
import math


def windowed_average(buffer, window_size=11):
    """
    Calculates the windowed average of the buffer.
    :param buffer: List of data to be smoothed out
    :param window_size: Amount of samples in the the averaging window
    :return: Windowed average.
    :raises: Assertion error, if window size is larger than the buffer itself
    """
    assert(window_size < len(buffer))
    result = []
    for i in range(len(buffer)):
        idx = (i - math.floor(window_size/2)) % len(buffer)
        i1 = (i + math.ceil(window_size/2)) % len(buffer)
        avg = 0
        while idx != i1:
            avg += buffer[idx]
            idx = (idx+1) % len(buffer)
        result.append(avg / window_size)
    return result


def weighted_windowed_average(fovs, size=11, power=2):
    # TODO: Find out what this exactly does and does it do what I think it does
    if size % 2 == 0:
        raise NotImplementedError
    midway = math.floor(size/2)
    weights = [pow(size - abs(midway - i), power) for i in range(size)]
    weight_sum = sum(weights)
    i0 = len(fovs) - math.floor(size/2)
    i1 = math.floor(size / 2)
    weighted_average = []
    for i in range(len(fovs)):
        if i0 > i1:
            window = fovs[i0:] + fovs[:i1+1]
        else:
            window = fovs[i0:i1+1]
        np_window = np.array(window)
        np_weights = np.array(weights)
        weighted_average.append(sum(np_window * np_weights) / weight_sum)
        i0 = (i0 + 1) % len(fovs)
        i1 = (i1 + 1) % len(fovs)
    return weighted_average

