"""
This script plots the data found from the database for each polar angle.

Or in other words...

The script will plot 4 figures, each figure has 25 plots.

Each plots has a scatter plot of all the raw measurements for corresponding angle (black)
and the result of Linear Regression if the dates and speeds are fitted against these angles (red).
The title of each plot will show the polar angle of the plot and the coefficients of the regression
first being the coefficient of the timestamp and second being the coefficient of the sphere speed.
"""

import sqlite3
from matplotlib import pyplot as plt
from future_fov import get_polars, get_fovs, get_estimator, estimate
import numpy
import itertools

db = sqlite3.connect("visiondata.db")
polars = get_polars(db)
all_fovs = []
all_timestamps = []
all_estimators = []
all_estimations = []

for p in polars:
    fovs, timestamps, speed = get_fovs(db, p)
    estimator = get_estimator(fovs, speed, timestamps)
    firstNlast = lambda l: [l[0], l[-1]]
    estimation = estimate(firstNlast(timestamps), firstNlast(speed), estimator)
    all_estimators.append(estimator)
    all_estimations.append(estimation)
    all_fovs.append(fovs)
    all_timestamps.append(timestamps)


rows = 5
columns = 5
polars = numpy.rad2deg(polars)
figure = plt.figure()
a0 = polars[0]
a1 = polars[24]
figure.suptitle("Angles {0:.2f}° - {1:.2f}°".format(a0, a1))
axs = figure.subplots(rows, columns)
axs = list(itertools.chain.from_iterable(axs))
tmp = 0

for i in range(len(polars)):
    ax = axs[tmp]
    f = all_fovs[i]
    ts = all_timestamps[i]
    ax.plot(ts, f, 'ko:', markersize=3)
    ax.plot([ts[0], ts[-1]], all_estimations[i], 'r-')
    ax.set_xticks([])
    ax.set_title("{0:.2f}°\n({1:.2E}, {2:.3f})".format(polars[i], *all_estimators[i].coef_))
    ax.set_ylim((4, 15))
    tmp += 1
    if tmp % 25 == 0:
        plt.show()
        figure = plt.figure()
        if i+1 < len(polars):
            a0 = polars[i+1]
            a1 = polars[i+25]
            figure.suptitle("Angles {0:.2f}° - {1:.2f}°".format(a0, a1))
            axs = figure.subplots(rows, columns)
            axs = list(itertools.chain.from_iterable(axs))
            tmp = 0


