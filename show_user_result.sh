#!/bin/bash

echo "Fetching files"

data_files=$(adb shell ls /sdcard/Android/data/com.TukeCode.MapMyVision/files/*.csv)
for file_path in $data_files; do
  filename="$(basename -- "$file_path")"
  adb pull "$file_path" "./datafiles/"
  echo "Draw!"
  python3 draw_test_result.py --draw-single="./datafiles/${filename}"
  read -p "Do you want to delete these files (y/n)?" answer
  case "$answer" in
   y|Y )
   echo "Deleting files..."
   adb shell rm /sdcard/Android/data/com.TukeCode.MapMyVision/files/"${filename}"
   echo "File removed from the headset"
   rm ./datafiles/"${filename}"
   echo "File removed from this computer"
   ;;
   * ) echo "Files kept";;
 esac
done

