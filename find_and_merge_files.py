"""
This script will search from N directories for files with same date prefix in the filename and
calculates the averages of those files, if there are exactly two files with the same prefix.
"""
from merge_data_files import average
import argparse
import glob
import os

# Todo: change the implementation so that it will certainly pair data from different directories
# Todo: Instead of working with files, use the database instead.


def find_and_merge_files(input_directories, output_directory):
    """
    Concatenates the csv files from the input directories
    and merges the data from these files into output directory
    :param input_directories: Source directories
    :param output_directory: Destination directory
    """
    input_files = []
    # Find files to merge
    for in_dir in input_directories:
        input_files += glob.glob(in_dir+"*.csv")
    files2merge = find_files2merge(input_files)
    merge_files(files2merge, output_directory)


def find_files2merge(input_files):
    """
    Builds a dictionary, where all the files dated to certain date are listed under each date
    :param input_files: List of source files
    :return: Dictionary where each prefix date is associated to list of corresponding source files
    """
    files2merge = {}
    for path in input_files:
        filename = os.path.basename(path)
        prefix_date = filename.split("_")[0]
        if prefix_date not in files2merge:
            files2merge[prefix_date] = []
        files2merge[prefix_date].append(path)
    return files2merge


def merge_files(files2merge, output_directory):
    """
    Loops trough a dictionary of files (see find_files2merge). If there are exactly two files, the
    function will merge them by calculating the average fovs per polar coordinate.
    :param files2merge: Dictionary, where kye is the file's date
    :param output_directory: Directory where the resulting file will be stored.
    :return:
    """
    skip_counter = 0
    merge_counter = 0
    for prefix, files in files2merge.items():
        if len(files) == 2:
            output_file = "{}{}_avg.csv".format(output_directory, prefix)
            if os.path.exists(output_file):
                continue
            else:
                skip_counter += 1
                average(*files, output_file)
        elif len(files) > 2:
            merge_counter += 1
    print("{} merge files created. {} skipped, because there were more than 2 files to merge."
          .format(skip_counter, merge_counter))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input-directories", nargs='+',
                        help="Specifies the directories, where script will search for files. Will use \"./appearing_data/\" "
                             "and \"./disappearing_data/\" by default.")
    parser.add_argument("--output-directory", help="Specifies directory where script will write the merged"
                                                              "files. Will use \"./merged_data/\" by default")
    args = parser.parse_args()
    input_directories = ["./appearing_data/", "./disappearing_data./"]
    output_directory = "./merged_data/"
    try:
        if args.input_directories is not None:
            input_directories = args.input_directories
        if args.output_directory is not None:
            output_directory = args.output_directory
        find_and_merge_files(input_directories, output_directory)
    except Exception as e:
        print(e)
