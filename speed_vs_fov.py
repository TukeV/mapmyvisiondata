"""
Quick script which helps me to investigate how projectile speed affects the data set and investigate if
there's a some kind of relationship between projectile speed measured range of vision.
"""
import sqlite3
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
import datetime
from numpy import rad2deg
import numpy as np

# Fetch the data from the DB
db = sqlite3.connect("visiondata.db")
cursor = db.cursor()
command = "SELECT Round(MEASUREMENTS.Polar, 2), MEASUREMENTS.FoV, " \
          "SESSIONS.Speed, SESSIONS.TimeStamp, SESSIONS.StartingAngle " \
          "FROM MEASUREMENTS LEFT JOIN SESSIONS ON SESSIONID=ID " \
          "WHERE SESSIONS.Direction=0"

data = cursor.execute(command).fetchall()
polars, fov, speed, ts, starting_angle = tuple(map(list, zip(*data)))
timestamp = list(map(lambda t: datetime.datetime.strptime(t, "%Y-%m-%d %H:%M:%S"), ts))
polars = rad2deg(polars)

# Calculate the actual projectile speed
# speed = np.divide(starting_angle, ttt)

# The first figure is a scatter plot on polar coordinates. Each marker is colored based on the sphere speed.
from matplotlib import colors as mplcolors
from matplotlib import cm

norm = mplcolors.Normalize(min(speed), max(speed))
norm_colors = norm(speed)
colors = plt.cm.jet(norm_colors)

figure = plt.figure()
axis = figure.add_subplot(111, projection='polar')
axis.scatter(polars, fov, c=colors)
figure.set_facecolor("grey")
axis.set_facecolor("xkcd:dark grey")
cbar = figure.colorbar(cm.ScalarMappable(norm=norm, cmap='jet'), ax=axis)
cbar.ax.set_ylabel("Speed (°/s)")
plt.grid(True)
plt.show()

figure = plt.figure()

# Next figure has two subplots.

# First subplot plots the relationship between sphere speed and measured FoV
axis = figure.add_subplot(211)
axis.plot(speed, fov, "o")
axis.set_xlabel("Speed (°/s)")
axis.set_ylabel("FoV")
axis.set_ylim(bottom=0)
axis.grid(True)

# Second subplot shows the relationship between FoV's time times they were measured.
# The subplot also has a line plot, which shows the speed of the spheres used on each date

axis = figure.add_subplot(212)
ax2 = axis.twinx()
axis.plot(timestamp, fov, "o")
ax2.plot(timestamp, speed, "kx:", markersize=5)
axis.set_xlabel("Timestamp")
axis.set_ylabel("FoV")
ax2.set_ylabel("Speed (°/s)", color="k")
axis.xaxis_date()
month_fmt = mdates.DateFormatter("%d.%m.%y")
axis.xaxis.set_major_formatter(month_fmt)
axis.xaxis.set_major_locator(mdates.MonthLocator())
axis.set_ylim(bottom=0)
axis.grid(True)

plt.show()
