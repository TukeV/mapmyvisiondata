import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from sklearn.linear_model import LinearRegression
import datetime
from dateutil.relativedelta import relativedelta
from windowing_algorithms import windowed_average, weighted_windowed_average
import argparse


def get_polars(connection):
    """
    Fetches unique polars from the database. This should yield the 100 polar angles used in my testing.
    :param connection: sqlite's database object
    :return: list of unique polar angles found from the database. Presumably the length of the list is 100
    """
    cursor = connection.cursor()
    command = "SELECT DISTINCT Round(Polar, 2) FROM MEASUREMENTS ORDER BY MEASUREMENTS.Polar"
    cursor.execute(command)
    polar_angles = [p[0] for p in cursor.fetchall()]
    return polar_angles


def get_fovs(connection, polar, direction=0):
    """
    Fetches fovs related to given polar. Direction parameter will filter the results based on test type.
    :param connection: sqlite's database object
    :param polar: The polar angle used to filter the search results.
    :param direction: Which direction the test was taken against. 0 for appearing spheres, 1 for disappearing
    :return: FoV angles related to given FoV
    """
    cursor = connection.cursor()
    command = "SELECT MEASUREMENTS.FoV, SESSIONS.Timestamp, SESSIONS.Speed " \
              "FROM MEASUREMENTS LEFT JOIN SESSIONS ON SessionID=ID " \
              "WHERE SESSIONS.Direction=? AND Round(Polar, 2)=Round(?, 2) AND SESSIONS.Size = 0.2 "
    cursor.execute(command, (direction, polar))
    data = cursor.fetchall()
    fovs, timestamps, speed = tuple(map(list, zip(*data)))
    timestamps = list(map(lambda t: datetime.datetime.strptime(t, "%Y-%m-%d %H:%M:%S").timestamp(), timestamps))
    return fovs, timestamps, speed


def get_estimator(fovs, speed, timestamps):
    """
    Creates a Linear Regression object based on measured FoV, sphere speed used during th test and timestamps.
    :param fovs: Fov related to same polar angle
    :param speed: Speed of the projectile
    :param timestamps: Timestamps used in the regression algorithm.
    :return: Sk-learn's estimator. Implementation details can vary
    """
    reg = LinearRegression()
    X = np.column_stack((timestamps, speed))
    Y = np.array(fovs)
    reg.fit(X, Y)
    return reg


def get_date_points():
    """
    Generates set of timestamps, for given time interval.
    :return: List of datetime objects
    """
    iter = datetime.datetime(2020, 1, 1)
    last_date = iter + relativedelta(years=21)
    interval = relativedelta(months=1)
    timestamps = []
    while iter < last_date:
        timestamps.append(iter.timestamp())
        iter += interval
    return timestamps


def estimate(timestamps, speeds, estimator):
    """
    Creates predictions based on timestamps and speeds using the estimator.
    :param timestamps: Time points to be estimated
    :param speeds: Speed of the projectile. This required because the estimator
    model used the projectile speed as factor, but in real life there's no speed to factor in.
    :param estimator: Estimator used for this polar angle
    :return: The list of angles (L values in polar coordinate system) estimated by the estimator.
    """
    X = np.column_stack((timestamps, speeds))
    estimated_fovs = estimator.predict(X)
    estimated_fovs = np.clip(estimated_fovs, a_min=0, a_max=12)
    # Make sure the FoVs are decreacing and never increacing
    for i in range(1, len(estimated_fovs)):
        estimated_fovs[i] = min(estimated_fovs[i-1], estimated_fovs[i])
    return estimated_fovs


def draw(polars, fovs, dates, export_animation=False):
    """
    Draws the resulting animation
    :param polars: Polar angles
    :param fovs: List of fov predictions in the near future
    :param dates: Dates for the FoV predictions
    """
    # Generator for the data
    def data_gen():
        for fov, date in zip(fovs, dates):
            yield (fov, date)

    # Init called by FuncAnimation function
    def init():
        del xdata1[:]
        del ydata1[:]
        del xdata2[:]
        del ydata2[:]
        lines[0].set_data(xdata1, ydata1)
        #lines[1].set_data(xdata2, ydata2)
        # ax.legend(bbox_to_anchor=(1.4, 1.), loc="upper right")
        figure.set_facecolor("grey")
        ax.set_facecolor("xkcd:dark grey")
        plt.yticks(fontsize=15, c="white")
        plt.xticks(fontsize=20, c="black")
        return lines,

    # Initialize required objects
    figure = plt.figure(figsize=[12, 8])
    ax = figure.add_subplot(111, projection='polar')
    line1, = ax.plot([], [], "k.--", label="Windowed Estimate", markersize=12, linewidth=3)
    line2, = ax.plot([], [], "rx", label="Unfiltered Estimate")
    xdata1, ydata1 = [], []
    xdata2, ydata2 = [], []
    lines = [line1, line2]

    # To complete the circle, the first value needs to be added to the end of the buffer
    polars.append(polars[0])

    # Work done on each cycle
    def run(data):
        # Remove previous fill of green
        ax.collections.clear()
        f, d = data
        # Smooth the predictions with windowed average
        f_windowed = windowed_average(f, 7)
        # To complete the circle, the first value needs to be added to the end of the buffer
        if len(f) == 100:
            f.append(f[0])
            f_windowed.append(f_windowed[0])
        xdata1 = xdata2 = polars
        ydata1 = f_windowed
        ydata2 = f
        lines[0].set_data(xdata1, ydata1)
        # lines[1].set_data(xdata2, ydata2)
        # Update title to show date of the frame
        dt = datetime.datetime.fromtimestamp(d)
        title = dt.strftime("%d.%m.%Y")
        ax.set_title(title, size=25)
        # Update fill
        ax.fill_between(polars, f_windowed, color="green")
        return lines,
    # Let the FuncAnimation do the work
    vision_animation = animation.FuncAnimation(figure, run, data_gen, init_func=init, save_count=6000)
    plt.show()
    # Save the animation as mp4
    if export_animation:
        filename = f'FoV-estimation-{datetime.datetime.now().strftime("%Y%m%d-%H%M")}.mp4'
        print(f'Export the animation to "{filename}"')
        writer = animation.FFMpegWriter(fps=30)
        vision_animation.save(filename, writer=writer)


def main(export_animation=False):
    connection = sqlite3.connect("visiondata.db")
    polar_angles = get_polars(connection)
    estimators = []
    for polar in polar_angles:
        fovs, timestamps, speed = get_fovs(connection, polar)
        estimator = get_estimator(fovs, speed, timestamps)
        estimators.append(estimator)
    prediction_dates = get_date_points()
    speeds = [sum(speed)/len(speed)] * len(prediction_dates)
    predicted_fovs = []
    for estimator in estimators:
        fovs = estimate(prediction_dates, speeds, estimator)
        predicted_fovs.append(fovs)
    predicted_fovs = list(map(list, zip(*predicted_fovs)))
    draw(polar_angles, predicted_fovs, prediction_dates, export_animation)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--export", help="With this flag, the script will export the created animation as mp4", required=False)
    args = parser.parse_args()
    export = args.export if args.export is not None else False
    main(export)
