<#
.SYNOPSIS
This is script is an utility scirpt, which automates several processes related to MapMyVision system.

.DESCRIPTION
This utility scripts is meant to be a single command, which performs all the required processes related to MapMyVisionSystem. This script includes things like fetching and removing data from the Oculus Go headset, storing the data files into the directories and the database and drawing the most recent data visualizations.

.PARAMETER all
With this switch, the script will perform all the normal tasks related to project: It will first fetch and store the data found from the VR headset and then run the visualization scripts. In the end, the script will also power off the headset device.

.PARAMETER visualize
When set true, the script will only run the data visualization scripts.

.PARAMETER store
When set true, the script will fetch and store the data, but will not run the visualization scripts.

.PARAMETER fetch
When set True, the script will try to find and copy the data from the Oculus Go headset to the host device. The script uses ADB commands to eprform these tasks, so ADB must be installed and set to the $PATH environmental variable. The command will copy all the files from headset to the ./Data/ directory, located in the current working directory.

.PARAMETER add2db
When set true, a python script will search all the files in the ./Data/ directory and add them to the database file, located in the current working directory (visiondata.db).

.PARAMETER copy
When set true, a python script will search trough all the files in ./Data directory and copy files to the /appearidng_data/ and /disappearing_data/ directories, depending on the header values in those files.

.PARAMETER averages
When set true, a python script will locate files dated for same date in /appearing_data and disappearing_data directories and create a file containging the averages calculated from these files. This file will be put in the ./averages directory.

.PARAMETER draw
When set true, a python script will draw the most recent test result. The script will draw the corresponding date from appearing_data, disappearing_data and averages.

.PARAMETER monthly
When set true, the script will draw the summary of the test results for the last 30 days.

.PARAMETER future_progression
With this parameter, the script will draw a simple linear regression from the collected data. The script will calculate the size of the visible area for all the test results thus far (appearing, disappearing and averages) and then calculates the trendlines with the linear regression algorithm.

.PARAMETER animate_future
With this parameter, the script will draw an animation which shows how the visible area will exactly diminish over course of 10 years. This algorithm calculates a trendline for each angle used in the testing instead of using the area, so the results will be slightly different compared to the future_progression command.

.PARAMETER results_per_feature
When enabled, the script will show a 3D graph of how the sphere size and speed affect the measured visible area.

.PARAMETER cleanup
With this parameter the script will remove all the files inside the headset.

.PARAMETER poweroff
When set true, the script will turn the VR device off at the end of the script by using adb shell.

#>


param (
  [switch] $all = $false,
  [switch] $store = $false,
  [switch] $visualize = $false,
  [switch] $fetch = $false,
  [switch] $cleanup = $false,
  [switch] $copy = $false,
  [switch] $averages = $false,
  [switch] $add2db = $false,
  [switch] $draw = $false,
  [switch] $monthly = $false,
  [switch] $future_progression = $false,
  [switch] $animate_future = $false,
  [switch] $results_per_feature = $false,
  [switch] $poweroff = $false
)


$ErrorActionPreference = "Stop"


# Shortcut for everything. 
if($all){
  $fetch = $true
  $copy = $true
  $add2db = $true
  $averages = $true
  $draw = $true
  $monthly = $true
  $future_progression = $true
  $animate_future = $true
  $results_per_feature = $true
  $cleanup = $true
  $poweroff = $true
}

# Shortcut for fetching and storing the data, but will not run visualizations
if($store){
  $fetch = $true
  $copy = $true
  $add2db = $true
  $averages = $true
  $cleanup = $true
}

# Shortcut for all visaulisations
if($visualize){
  $draw = $true
  $monthly = $true
  $future_progression = $true
  $animate_future = $true
  $results_per_feature = $true
}


if($fetch -or $cleanup){
  adb start-server
}

# Get files from the Oculus Go
if($fetch){
    
    Write-Host "Fetching files from Oculus Go"
	$success = $false
	$files = adb shell ls /sdcard/Android/data/com.TukeCode.MapMyVision/files
	foreach($file in $files){
	  if($file.Split(".") -Contains "csv"){
		$path = "/sdcard/Android/data/com.TukeCode.MapMyVision/files/{0}" -f $file
		adb pull $path ".\Data\"
		$success = $true
	  }
	}
	
	if(!$success){
  	   if($cleanup){
	     Write-Error "The script was configured to delete the files from the device after they have been retrieved. However, the pull script was not able to pull a single file.`nThe script will now terminate to make sure you don't accidentally delete non-saved files."
	     exit
	   }else{
	     Write-Warning "No files found from the device."
	   }
	}
}


# Add to database
if($add2db){
    Write-Host "Adding files to the database"
	python csv2db.py --folder ./Data
    Write-Host "Pre-calculate windowed averages"
    python wa2db.py
}

# Add copies to appearing_data and disappearing_data directories based on file headers.
if($copy){
    Write-Host "Copying files based on their Direction Header"
	$AllFiles = Get-ChildItem ./Data/*.csv | Select-Object Name
	$SortedFiles = (Get-ChildItem ./appearing_data/*.csv) + (Get-ChildItem ./disappearing_data/*.csv) | Select-Object Name
	$NewFiles = $AllFiles | Where-Object {$SortedFiles.Name -notcontains $_.Name} | Select-Object Name

	foreach($item in $NewFiles){
	  $filename = $item.Name
	  $filepath = "./Data/$filename"
	  If(Get-Content $filepath | Select-String "Direction: In"){
		Copy-Item $filepath ".\appearing_data\$filename"
		Write-Host "$filename copied to /appearing_data/ directory"
	  }ElseIf(Get-Content $filepath | Select-String "Direction: Out"){
        Copy-Item $filepath ".\disappearing_data\$filename"
		Write-Host "$filename copied to /disappearing_data/ directory"
	  }Else{
		Write-Warning "$filename does not have direction header."
	  }
	}
}

# Calculate averages
if($averages){
  Write-Host "Invoking find_and_merge_files.py" 
  python find_and_merge_files.py
}

# Draw the latest result
if($draw){
  Write-Host "Drawing the most recent results"
  python draw_most_recent.py
}

# Draw the average of the last 30 days of measuring
if($monthly){
    Write-Host "Drawing the average of the measurements in last 30 days"
    python monthly_results.py
}

# Draw the visible area graphs
if($future_progression){
  Write-Host "Drawing the estimated progression"
  python estimate_fov_area.py
}

# Draw the animated version of the visible area
if($animate_future){
  Write-Host "Drawing the estimated results of Appearing Spheres test in near future"
  python future_fov_plus_area.py
}

# Draw a 3D scatter plot of speed, sphere size and measured area
if($results_per_feature){
    Write-Host "Regression analysis with color coded parameters"
    python regression_n_parameters.py
    Write-Host "Showing the 3D graph of spehere speed, size and measrued area"
    python 3d_plot_4_parameters.py

}

# Remove all csv files from Oculus Go
if($cleanup){
  Write-Host "Removing files from Oculus Go"
  adb shell rm /sdcard/Android/data/com.TukeCode.MapMyVision/files/*.csv
}

# Poweroff the device over adb
if($poweroff){
  Write-Host "Headset will now power off"
  adb shell reboot -p
}