import argparse
import pandas


def merge(input_files, output_file):
    """
    Merges all the input files into a single output file
    :param input_files: List of paths of files to be combined
    :param output_file: Path to destination where the merged results will be saved
    """
    frames = []
    for file in input_files:
        data_frame = pandas.read_csv(file, comment="/")
        frames.append(data_frame)
    result = pandas.concat(frames, sort=True)
    result.to_csv(output_file)


def average(file1, file2, output_file):
    """
    Takes the averages of two input files and stores the result to the output file. The average is calculated as per.
    :param file1: First source file
    :param file2: Second source file
    :param output_file: Path where the resulting averages will be stored
    """
    with open(file1, 'r') as f1, open(file2, 'r') as f2, open(output_file, 'w') as fout:
        # Writing Header
        fout.write("// This file has been created by combining files {} and {}\n".format(file1, file2))
        fout.write("// PolarAngle, Radius, FovAngle\n")
        for line1, line2 in zip(f1, f2):
            if line1[0] == '/': continue
            data1 = [float(x) for x in line1.split(',')]
            data2 = [float(x) for x in line2.split(',')]
            data3 = [(x+y)/2.0 for x, y in zip(data1, data2)]
            fout.write("{}, {}, {}\n".format(*data3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", nargs='+', type=str, help="Files to be merged", required=True)
    parser.add_argument("-o", "--output", type=str, help="Merged output file", required=True)
    parser.add_argument("--avg", help="use this flag to take average between two files", action='store_true')
    args = parser.parse_args()
    print(args)
    print(args.input, "-->" , args.output)
    if args.avg:
        if len(list(args.input)) == 2:
            average(args.input[0], args.input[1], args.output)
        else:
            print("There must be only two input files")
    else:
        merge(args.input, args.output)
