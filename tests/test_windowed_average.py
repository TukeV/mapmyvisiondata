import unittest
from windowing_algorithms import windowed_average


class WindowedAverageTest(unittest.TestCase):
    def test_even_sized_window(self):
        buffer = [1, 9, 2, 8, 3, 7, 4, 6, 5, 0]
        window_size = 4
        # Correct answer calculated with pen and paper
        correct_answer = [3.75, 3.0, 5.0, 5.5, 5.0, 5.5, 5.0, 5.5, 3.75, 3.0]
        answer = windowed_average(buffer, window_size)
        self.assertListEqual(correct_answer, answer)

    def test_odd_sized_window(self):
        buffer = [1, 9, 2, 8, 3, 7, 4, 6, 5, 0]
        window_size = 5
        # Correct answer calculated with pen and paper
        correct_answer = [3.4, 4.0, 4.6, 5.8, 4.8, 5.6, 5.0, 4.4, 3.2, 4.2]
        answer = windowed_average(buffer, window_size)
        self.assertListEqual(correct_answer, answer)


if __name__ == '__main__':
    unittest.main()
