import sqlite3
import re
import datetime
import glob
import os
import argparse


def file_exists(connection, sourcefile):
    """
    Returns True if file is already in the SESSIONS table
    :param connection: Sqlite database connection
    :param sourcefile: name of the source file
    :return: True if file is found in the Sessions table, otherwise False
    """
    cursor = connection.cursor()
    command = "SELECT EXISTS(SELECT 1 FROM SESSIONS WHERE source=?)"
    result = cursor.execute(command, (sourcefile,))
    return result.fetchone() == (1,)


def csv2db(filepath, connection):
    """
    Adds the file in the filepath to the database, if it's not already there
    :param filepath: Filepath
    :param connection: Sqlite database connection
    """
    basename = os.path.basename(filepath)
    if file_exists(connection, basename):
        # File is already in the database
        return
    cursor = connection.cursor()
    timestamp = datetime.datetime.strptime(basename, "%y%m%d_%H%M_spheres.csv")
    data_id = None
    print("Adding contents of {} into the database...".format(filepath), end='')
    with open(filepath, 'r') as source_file:
        header = True
        for line in source_file:
            if header and line[0] == '/':
                # First lines of the file are metadata, which goes to the SESSIONS table

                # IN means that it's appearing spheres test. This test is marked with direction value of 0
                # OUT means that it's disappearing spheres test. This test is marked with direction value of 1
                direction = 0 if re.findall(r"(In|Out)", line)[0] == "In" else 1
                distance = int(re.findall(r"Distance: (.\d+)", line)[0])
                time2travel = re.findall(r"TimeToTravel: (.\d+\.\d+|\d+)", line)
                speed = re.findall(r"Speed: (\d+\.\d+|\d+)", line)
                size = re.findall(r"Size: (\d+\.\d+|\d+)", line)
                if len(time2travel) != 0:
                    t2t = float(time2travel[0])
                    speed = round(distance/t2t, 3)
                else:
                    speed = float(speed[0])
                size = float(size[0])
                insert_command = "INSERT INTO 'SESSIONS' " \
                                 "('Timestamp', 'Direction', 'Speed', 'StartingAngle', 'Source', 'Size') " \
                                 "VALUES (?, ?, ?, ?, ?, ?);"
                data = (timestamp, direction, speed, distance, basename, size)
                cursor.execute(insert_command, data)
                data_id = cursor.lastrowid
                header = False
            elif line[0] != "/":
                # Maybe it would be smarter to first read all the measurement data and
                # then add everything with one SQL command instead of adding everything line by lie
                polar, _, fov = [float(x) for x in line.split(',')]
                insert_command = "INSERT INTO MEASUREMENTS (SessionID, Polar, FoV) VALUES (?, ?, ?);"
                data = (data_id, polar, fov)
                cursor.execute(insert_command, data)
        connection.commit()
        print('Done')


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", nargs="+", type=str, required=False, help="List of files to be added into database.")
    parser.add_argument("--folder", type=str, required=False, help="All the files inside this folder will be added to the database")
    return parser.parse_args()


if __name__ == '__main__':
    try:
        args = get_arguments()
        # noinspection SpellCheckingInspection
        conn = sqlite3.connect("visiondata.db")
        if args.files is not None:
            for file_path in args.files:
                csv2db(file_path, conn)
        if args.folder is not None:
            data_files = glob.glob("{}/*_spheres.csv".format(args.folder))
            for file_path in data_files:
                csv2db(file_path, conn)
        if args.folder is None and args.files is None:
            print("Using default folder \"./Data\"")
            data_files = glob.glob("{}/*_spheres.csv".format("./Data"))
            for file_path in data_files:
                csv2db(file_path, conn)
    except Exception as e:
        print(e)
